cmake_minimum_required(VERSION 3.5.0)
project(pydme)

if(WIN32)
    set(DolphinProcessSrc DolphinProcess/Windows/WindowsDolphinProcess.cpp)
endif(WIN32)

if(UNIX)
   set(DolphinProcessSrc DolphinProcess/Linux/LinuxDolphinProcess.cpp)
endif(UNIX)

set(SRCS ${DolphinProcessSrc}
         DolphinProcess/DolphinAccessor.cpp
         Common/MemoryCommon.cpp
         MemoryWatch/MemWatchEntry.cpp
         MemoryScanner/MemoryScanner.cpp
)

set(CMAKE_INCLUE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD_REQUIRED YES)
set(CMAKE_CXX_STANDARD 14)

add_library(pydme ${SRCS})